/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/

/*
 * Definitions that should not be in public / installed header
 */
#ifndef tplog_local_h
#define tplog_local_h

/* Stringify */
#undef _XSTR
#undef STR
#define _STR(x) #x
#define STR(x)  _STR(x)

/* Our version of liblog ASSURE macro so we dont have to include both liblog
 * and tplog and break our own rule */
#define ASSURE(X)                                                           \
    if (!(X)) {                                                             \
        fprintf(stderr, "ASSURE fail at " __FILE__ " line %d\n", __LINE__); \
    }

#ifndef UNUSED
#    define UNUSED(X) ((void)(X))
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef enum { LOGTYPE_ANDROID, LOGTYPE_LINUX } tplog_logtype;
struct tplog_settings {
    tplog_logtype logtype; /* Android or Linux-kernel style decoration */
    bool deco_probeid;     /* Decorate including probe-ID for convenience */
};
extern struct tplog_settings *__tplog_settings;

/* Portable integer types that can hold an address. Signed or not is only
  meaningful for address deltas */
typedef intptr_t iptr;
typedef uintptr_t uptr;

/* Module initializer counter */
int tplog_init_cnt(); /* Module initializer counter */

const char *tplog_version();
void tplog_register_lbms();
void tplog_open_printfd(const char *fname);
FILE *tplog_fdbg();
char tplog_letter(int);

#endif // tplog_local_h
