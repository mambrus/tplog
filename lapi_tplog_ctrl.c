/***
Lua API for `utrace`

`utrace` stands for uC-tracer and is a light-weight tracer for C which works
on everything from bare-silicon deeply embedded systems, to high-end OS:es
like Linux. Central part of `utrace` are it's probes. With probes in place,
SW can be not only traced, but also extend loggers like `liblog` to have
their other-wise compile-time behaviour hard-coded in, to become modified in
run-time. It can probably be made to operate as normal Lua module but was
designed to integrated into a Lua-VM (see xb-lua below).

This module is a refined external API for `utrace` that has it's own native
lower-level API here:
- [utrace-probe](http://www.ambrus.se/~michael/ldoc/utrace/modules/utrace-probe.html)
- [utrace-rpc](http://www.ambrus.se/~michael/ldoc/utrace/modules/utrace-rpc.html)

## See also (project home-sites)

- [liblog](https://gitlab.com/mambrus/liblog)
- [utrace](https://gitlab.com/mambrus/utrace)
- [xb-lua](https://gitlab.com/mambrus/xb-lua)
- [tplog](https://gitlab.com/mambrus/tplog)


@module utrace
@author Michael Ambrus <michael@ambrus.se>
@copyright &copy; Michael Ambrus 2024
@license MIT
*/
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <tplog/log.h>
#include <utrace/utrace.h>
#include <xb-lua/luabind.h>

#include "local.h"

static bool dummy(log_level ll, const char *msg)
{
    UNUSED(ll);
    UNUSED(msg);
    fprintf(stderr,
            "ERROR: No call-back function has been set. Can't send_log\n");
    return false;
}
typedef bool (*send_log_t)(log_level, const char *);
static send_log_t send_log = dummy;
void set_send_log(send_log_t fun)
{
    send_log = fun;
}

/***
version string

@function version
@treturn string version
*/
static int l_version(lua_State *L)
{
    lua_pushstring(L, tplog_version());

    return 1;
}

/***
Get tplog settings into a "json" table

Each table index is a tuple:
["name" : value]

@usage
Each tuple in settings-table:
--[[
1. (enum)   logtype         0=LOGTYPE_ANDROID, 1=LOGTYPE_LINUX
2. (bool)   deco_probeid    Additional decoration (wrt liblog) including
                            probe-ID for convenience
--]]

@usage
-- Example:

n,s = ulog.settings()
for i=1,n do print(s[i][1], s[i][2]) end

@function settings
@treturn integer number settings
@treturn table tuple-table with settings
*/
static int l_settings(lua_State *L)
{
    char *rs;
    UNUSED(L);
    UNUSED(rs);

    lua_pushinteger(L, 2);

    lua_newtable(L);
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "logtype");
        assert(rs);
        lua_rawseti(L, -2, 1);

        lua_pushinteger(L, __tplog_settings->logtype);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 1); /* Finalize row */
    }
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "deco_probeid");
        assert(rs);
        lua_rawseti(L, -2, 1);

        lua_pushinteger(L, __tplog_settings->deco_probeid);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 2); /* Finalize row */
    }
    return 2;
}

/***
Set tplog settings

Each argument has the same meaning and order as the table from settings()

@see settings

@function set_settings
@treturn boolean success
*/
static int l_set_settings(lua_State *L)
{
    int ival;
    bool is_good = true;

    ival = luaL_checkinteger(L, 1);
    if ((ival < 0) || (ival > 1)) {
        fprintf(stderr, "Arg1 invalid: %d\n", ival);
        is_good = false;
    }
    __tplog_settings->logtype = ival;

    ival = luaL_checkinteger(L, 2);
    if ((ival < 0) || (ival > 1)) {
        fprintf(stderr, "Arg2 invalid: %d\n", ival);
        is_good = false;
    }
    __tplog_settings->deco_probeid = ival;

    lua_pushinteger(L, is_good);
    return 1;
}

/***
Send log

Test-API to send a liblog log-message of certain type
Disable syslog or close it (if opened)

Which type determined by the first argument. Second argument enables copy
syslog to stderr. It's ignored if not enabling syslog operation.

@function send
@tparam integer  V=0, D=1, I=2, W=3, E=4, C=5
@tparam string Message

@treturn boolean OK
*/
static int l_send(lua_State *L)
{
    bool is_good = true;
    int ll;

    ll = luaL_checkinteger(L, 1);
    if ((ll < 0) || (ll > 5)) {
        fprintf(stderr, "Arg1 invalid: %d\n", ll);
        is_good = false;
    }

    if (!is_good) {
        lua_pushinteger(L, 0);
        return 1;
    }

    const char *msg = luaL_checkstring(L, 2);
    is_good = send_log(ll, msg);

    if (is_good)
        lua_pushinteger(L, 1);
    else
        lua_pushinteger(L, 0);

    return 1;
}

/***
Send log message

Test-API to send a tplog log-message of certain type. This API is routed
though tplog API with much more control and decoration liblog's native API.

Which type determined by the first argument.

@function log
@tparam string  "verbose"=0, etc
@tparam string Message

@treturn boolean OK
*/
static int l_log(lua_State *L)
{
    int is_good = true;
    const char *level = luaL_checkstring(L, 1);
    log_level ll = liblog_str2loglevel(level, &is_good);

    if (!is_good) {
        fprintf(stderr, "Arg1 invalid: %s\n", level);
        lua_pushinteger(L, 0);
        return 1;
    }

    const char *msg = luaL_checkstring(L, 2);
    is_good = send_log(ll, msg);

    if (is_good)
        lua_pushinteger(L, 1);
    else
        lua_pushinteger(L, 0);

    return 1;
}

/* Registration part below */
/* ------------------------------------------------------------------------- */

static const struct luaL_Reg this_lib[] = {
    /* clang-format off */
    { "version", l_version },
    { "settings", l_settings },
    { "set_settings", l_set_settings },
    { "send", l_send },
    { "log", l_log },
    { NULL, NULL }                   /* sentinel */
    /* clang-format on */
};

/* Lua or LBM uses this to register the library in a VM */
static int do_reg(lua_State *L)
{
    luaL_newlib(L, this_lib);
    return 1;
}

/* LBM uses this to register the library in a VM */
void bind_tplog_ctrl_library(lua_State *L)
{
    luaL_requiref(L, "ulog", do_reg, 1);
    lua_pop(L, 1);
}
