/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/
#define _GNU_SOURCE
#if __GNUC__ < 10
#    undef _FORTIFY_SOURCE
#    define _FORTIFY_SOURCE 0
#endif

#include "local.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tplog/log.h>
#include <utrace/probe.h>

#include "tplog_config.h"
#define DEBUGF tplog_fdbg()

static char *ppstr[2][8] = {
    { "[V]", "[D]", "[I]", "[W]", "[E]", "[C]", "[+]", "[*]" },
    { "<7>", "<6>", "<5>", "<4>", "<3>", "<2>", "<1>", "<0>" }
};

static char *pmstr[2][2] = {
    { " ", ">" }, /* "Is-controlled" marker Android-style logs */
    { " ", "+" }  /* "Is-controlled" marker Linux kernel style logs */
};

static struct tplog_settings settings = {
    .logtype = LOGTYPE_ANDROID,
    .deco_probeid = true,
};
struct tplog_settings *__tplog_settings = &settings;

/* Return the Android log-level character for a loglevel */
char tplog_letter(int ll)
{
    /* Early returns are errors but return something */
    if (ll < LOG_LEVEL_VERBOSE)
        return '-';
    if (ll > LOG_LEVEL_SILENT)
        return '+';

    return ppstr[LOGTYPE_ANDROID][ll][1];
}

/* Trace-point aware/conditional log-write. I.e. LOG-point executing this
   function is controlled by utrace (bit-mask)
 */
void tplog_write(struct probe_meta *tp, struct log_meta *lm, char *cformat, ...)
{
    va_list ap;
    va_start(ap, cformat);

    /* Interconnect if needed */
    if (!tp->piggy)
        tp->piggy = lm;

    log_level level = lm->ploglevel;

    char *buf;
    vasprintf(&buf, cformat, ap);

    /* <START> Decoration build-up */
    bool has_deco = false;
    char *dec0 = "";
    char *dec1 = "";

    if (settings.deco_probeid) {
        if (has_deco)
            asprintf(&dec0, " %p", tp);
        else
            asprintf(&dec0, "[%p", tp);
        has_deco = true;
    }

    /* Finish series of decorations */
    if (has_deco)
        asprintf(&dec1, "%s] ", dec0);

        /* <END> Decoration build-up */

#ifdef TPLOG_PRINTFDEBUG
    fprintf(DEBUGF, "[%p] %s %s", tp->data, ppstr[logtype][level], buf);
    fflush(DEBUGF);
#endif

    if (tp->data->status & UTRACE_LOG_CNTRL) {
        if (tp->data->status & UTRACE_DO_LOG) {
            /* If bit "UTRACE_DO_LOG" expressively is set, do log regardless
             * log-level. Otherwise forced silent */
            log_write(LOG_LEVEL_EXT, "%s%s %s%s",
                      ppstr[settings.logtype][level],
                      pmstr[settings.logtype][1], dec1, buf);
        }
    } else {
        log_write(level, "%s%s %s%s", ppstr[settings.logtype][level],
                  pmstr[settings.logtype][0], dec1, buf);
    }
    va_end(ap);
    free(buf);
    if (has_deco) {
        free(dec0);
        free(dec1);
    }
}
