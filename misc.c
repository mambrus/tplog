/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/
#include <string.h>
#include <tplog/lapi.h>
#include <xb-lua/luabind.h>

#include "tplog_config.h"
#include "local.h"

static const char TPLOG_VERSION_STR[] = TPL_PROJ_NAME " <" TPL_VERSION ">";
FILE *fdbg = NULL; /* File for printf-debugging the logger */

const char *tplog_version()
{
    return TPLOG_VERSION_STR;
}

#ifndef TPLOG_AUTOINIT
/* Stand-in when initfini.c is not auto-initializing */
static int init_cntr = 0;
int tplog_init_cnt()
{
    return init_cntr;
}
#endif

/*
 Register supported Lua language API's
*/
void tplog_register_lbms()
{
    ASSURE(lua_registerlibrary(bind_tplog_library) > 0);
    ASSURE(lua_registerlibrary(bind_tplog_ctrl_library) > 0);
    ASSURE(lua_registerlibrary(bind_liblog_ctrl_library) > 0);
}

void tplog_open_printfd(const char *fname)
{
    if (strcmp(fname, "/dev/stderr") == 0)
        fdbg = stderr;
    else if (strcmp(fname, "/dev/stdout") == 0)
        fdbg = stdout;
    else
        fdbg = fopen(fname, "a+");
}

FILE *tplog_fdbg()
{
    if (!fdbg)
        return stderr;
    return fdbg;
}
