/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/
/*
 * Call-back funtions for ulog.send()/ulog.mess()
 */

#include <stdbool.h>
#include <tplog/log.h>

bool test_send_log(log_level ll, const char *msg)
{
    bool is_good = true;

    switch (ll) {
    case LOG_LEVEL_VERBOSE:
        LOGV("%s\n", msg);
        break;
    case LOG_LEVEL_DEBUG:
        LOGD("%s\n", msg);
        break;
    case LOG_LEVEL_INFO:
        LOGI("%s\n", msg);
        break;
    case LOG_LEVEL_WARNING:
        LOGW("%s\n", msg);
        break;
    case LOG_LEVEL_ERROR:
        LOGE("%s\n", msg);
        break;
    case LOG_LEVEL_CRITICAL:
        LOGC("%s\n", msg);
        break;
    case LOG_LEVEL_SILENT:
    default:
        is_good = false;
        break;
    }
    return is_good;
}
