/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/

/*
 * Definitions that should not be in public / installed header
 */
#ifndef tplog_test_h
#define tplog_test_h

/* Stringify */
#undef _XSTR
#undef STR
#define _STR(x) #x
#define STR(x)  _STR(x)

/* Our version of liblog ASSURE macro so we dont have to include both liblog
 * and tplog and break our own rule */
#define ASSURE(X)                                                           \
    if (!(X)) {                                                             \
        fprintf(stderr, "ASSURE fail at " __FILE__ " line %d\n", __LINE__); \
    }

#ifndef UNUSED
#    define UNUSED(X) ((void)(X))
#endif

void *thread_A(void *inarg);
void *thread_B(void *inarg);
void *thread_C(void *inarg);
void logger_test();

/* Work-around for lua API not to optimaze away probe segments (FIXME) */
typedef bool (*send_log_t)(log_level, const char *);
void set_send_log(send_log_t fun);
bool test_send_log(log_level, const char *);

#endif // tplog_test_h
