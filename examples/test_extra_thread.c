/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/
#include <stdbool.h>
#include <time.h>
#include <tplog/log.h>

#include "tplog_test.h"
#include "examples_config.h"

void *thread_A(void *inarg)
{
    for (size_t i = 0; true; i++) {
        UPROBE();
        LOGI("Thread A enters: %d\n", i);
        usleep(1000 * EXAMPLE_BASE_DELAY_MS / 2);
        UPROBE();
        usleep(1000 * EXAMPLE_BASE_DELAY_MS / 2);
        LOGD("Thread A exits: %d\n", i);
    }
    return NULL;
}

void *thread_B(void *inarg)
{
    for (size_t i = 0; true; i++) {
        UPROBE();
        LOGI("Thread B enters: %d\n", i);
        usleep((1000 * EXAMPLE_BASE_DELAY_MS * 2.1) / 2);
        UPROBE();
        usleep((1000 * EXAMPLE_BASE_DELAY_MS * 2.1) / 2);
        LOGD("Thread B exits: %d\n", i);
    }
    return NULL;
}

void *thread_C(void *inarg)
{
    for (size_t i = 0; true; i++) {
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        LOGV("Thread C 1/2: %d\n", i);
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        UPROBE();
        usleep(1000 * EXAMPLE_BASE_DELAY_MS);
        LOGV("Thread C 2/2: %d\n", i);
    }
    return NULL;
}
