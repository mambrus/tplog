/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   Test new liblog C-runtime features                                    *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/
#include <tplog/log.h>
#include <utrace/utrace.h>

#include "tplog_test.h"
#include "tplog_config.h"
#include "examples_config.h"

void logger_test()
{
    /* File mode swap test (begin)*/
    const char *old_fname;

    old_fname = liblog_logfile("/dev/stdout", 0);
    LOGE("Hello stdout. Old fnaname: %s\n", old_fname);
    old_fname = liblog_logfile("/dev/stderr", 0);
    LOGE("Back to stderr. Old fnaname: %s\n", old_fname);

    LOGE("Re-routing all logs /tmp/log with various flush policies\n");
    LOGE("30 fully buffered messages is be written to file /tmp/log\n");
    LOGE("(You will probably not see any until all are written)\n");
    usleep(300000);

    old_fname = liblog_logfile("/tmp/log", 0);
    for (size_t i = 0; i < 30; i++) {
        LOGE("Synchronous #%d\n", i);
        usleep(300000);
    }

    old_fname = liblog_logfile("/dev/stderr", 0);
    LOGE("30 explicit flush each 5 messages to file /tmp/log\n");
    usleep(300000);

    old_fname = liblog_logfile("/tmp/log", 5);
    for (size_t i = 0; i < 30; i++) {
        if (((i+1)%5) == 0) {
            LOGE("Flush ed #%d <-- Flushed here\n", i);
        } else {
            LOGE("Flush ed #%d\n", i);
        }
        usleep(300000);
    }

    old_fname = liblog_logfile("/dev/stderr", 0);
    LOGE("5 Synchronous messages to file /tmp/log. Old file-name: %s\n",
         old_fname);
    usleep(300000);

    /* Negative count value means "flush-on-each" (Not recommended) */
    old_fname = liblog_logfile("/tmp/log", -1);
    for (size_t i = 0; i < 5; i++) {
        LOGE("Synchronous #%d\n", i);
        usleep(300000);
    }
}
