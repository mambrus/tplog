/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <tplog/lapi.h>
#include <tplog/log.h>
#include <utrace/utrace.h>
#include <xb-lua/console.h>
#include <xb-lua/envutil.h>
#include <xb-lua/luabind.h>

#include "tplog_test.h"
#include "tplog_config.h"
#include "examples_config.h"
#include "local.h"

static int port = TPLOG_DEFAULT_PORT;
static char *ifname = TPLOG_DEFAULT_IFNAME;

/*
 * Set-up logger to suite
 */
static void logger_init(char **argv)
{
    char *loglevel = NULL;
    char *procname = NULL;
    liblog_timezero_now();       /* Make time-staps relative now */
    liblog_syslog_config(false); /* All start-up logs to syslog, no echo */

    liblog_set_verbosity(LOG_LEVEL_SILENT);
    liblog_set_decorators(SHORT_TIME, TID_DECO);

    procname = strchr(argv[0], '/');
    /* Store name even if we're not using it */
    liblog_set_process_name(procname ? &procname[1] : argv[0]);
    liblog_set_process_name(NULL); // Remark to output process-name

    if (loglevel = getenv("TPL_LOGLEVEL")) {
        liblog_set_verbosity(liblog_str2loglevel(loglevel, NULL));
    } else {
        liblog_set_verbosity(LOG_LEVEL_INFO);
    }
}

int main(int argc, char **argv)
{
    pthread_t t;
    logger_init(argv);

#ifdef TPLOG_AUTOINIT
    ASSURE(tplog_init_cnt() > 0);
#else
    /* Pick and choose */
    ASSURE(lua_registerlibrary(bind_tplog_library) > 0);
    ASSURE(lua_registerlibrary(bind_tplog_ctrl_library) > 0);
    ASSURE(lua_registerlibrary(bind_liblog_ctrl_library) > 0);
#endif

    struct lua_vm_environment *vm_env = NULL;
    vm_env = lua_env_create("TPLOG", 0);

#ifdef JUST_TO_EXEMPLIFY
    vm_env = lvm_env_get(); /* Only needed if not using lua_env_create() */
#endif
    lvm_env_setstat(vm_env, "@" TPLOG_LIB_PATH "/init.lua",
                    "@" TPLOG_LIB_PATH "/connect.lua",
                    "@" TPLOG_LIB_PATH "/disconn.lua");

    lvm_env_init();
    ASSURE(luacon_vm_singlesession(port, ifname, stateful) == 0);

    usleep(300000);
    lvm_env_destroy(&vm_env);
    liblog_syslog_close(); /* All logs now to stderr */
    LOGI("tplog-test started services successfully\n");

    /* Store a CB in Lua API to test send_log */
    set_send_log(test_send_log);

    printf("tplog-test: Built in directory: %s\n", STR(__DIR__));
    printf("xb-lua console service is running. To attach, use:\n");
    printf("tcp-term -p %d\n", port);

#ifdef JUST_TO_EXEMPLIFY
    logger_test();
#endif
    LOGI("tcp-term -p %d\n", port);

    printf("Press any char to continue\n");
    fflush(NULL);
    getchar();

    pthread_create(&t, NULL, thread_A, NULL);
    pthread_create(&t, NULL, thread_B, NULL);
    pthread_create(&t, NULL, thread_C, NULL);
    printf("Code is running... Please probe me!\n");
    fflush(NULL);

    for (size_t i = 0; true; i++) {
        UPROBE();
        LOGI("ROOT enters: %d\n", i);
        usleep(1000 * EXAMPLE_BASE_DELAY_MS * 3.14);
        UPROBE();
        LOGV("ROOT LOGV: %d\n", i);
        usleep(1000 * EXAMPLE_BASE_DELAY_MS * 3.14);
        LOGD("ROOT exits: %d\n", i);
    }
    return 0;
}
