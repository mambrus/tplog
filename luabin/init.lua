--[[--
Connect/Disconnect initializion

Example for xb-lua's extended feature to treat connect/diconnect differently
from VM start-up.

@see tplog
@script init.lua
--]]
tplog_ncon=0
tplog_ndis=0
