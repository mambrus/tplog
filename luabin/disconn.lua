--[[--
Disconnect script

Disconnect script example for xb-lua which is run on each disconnect for a state-ful VM

@see tplog
@script disconn.lua
--]]
tplog_ndis = tplog_ndis + 1;
p_fn, p_mm, c_fn, c_mm = liblog.setlogfile(p_fn, p_mm);
print("Logs are restored:", c_fn, c_mm, p_fn, p_mm);
print("Disconnecting. Goodbye and thank-you for the fish...");


