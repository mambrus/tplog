--[[--
Connect script

Connect script example for xb-lua which is run on each disconnect for a
state-ful VM

Reroute logs from `stderr` to IO-mode (file) /tmp/log and get7stor
previous/current settings to be used to restore on disconnect

@see tplog
@script connect.lua
--]]
p_fn, p_mm, c_fn, c_mm = liblog.setlogfile("/tmp/tplog-test", -1);
print("tplog ctrl:", log.version());
print("tplog log: ", ulog.version());
print(probe.version());
print(liblog.version());
print();
print("Welcome to tplog, an utrace enhanced logger for C");
print("Logs are re-routed:", c_fn, c_mm, p_fn, p_mm);
print("You might want to try the following to get started:");

print("");
example_text=[=[
probe.listall()
log.listall()
N,P = probe.all()
for i = 1, N do
    print(i, P[i][1], P[i][2], P[i][3], P[i][4], P[i][5]);
end
N,P = log.all()
for i = 1, N do
    print(i, P[i][1], P[i][2], P[i][3], P[i][4], P[i][5]);
end
]=]
print(example_text);


print("");
tplog_ncon = tplog_ncon + 1;
print("Number of times connected/disconnected:", tplog_ncon, tplog_ndis);
print("To exit this TCP-connection: ^D ^C");
