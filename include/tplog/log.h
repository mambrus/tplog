/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice at the end of this file                          *
 *                                                                         *
 **************************************************************************/
/*
** $Id: log.h
** Plog - A post-deploy controllable C-code logger
** Author:     Michael Ambrus <michael@ambrus.se>
**             Helsingborg Sweden (http://www.ambrus.se/~michael)
*/
#ifndef tplog_h
#define tplog_h

#ifdef liblog_log_h
#    error Do not include both liblog/log.h and tplog/log.h. Either-or also chooses logger
#endif

#include <liblog/log.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <utrace/utrace.h>

struct log_meta {
    log_level ploglevel; /* Probe log-"kind" */
    log_level loglevel;  /* Used loglevel when controlled */
    int fd;              /* File-descriptor when contolled */
};

void tplog_write(struct probe_meta *, struct log_meta *, char *cformat, ...);

#define TP_LOG(KIND, ...)                        \
    {                                            \
        static struct log_meta lm = {            \
            .fd = STDERR_FILENO,                 \
            .ploglevel = LOG_LEVEL_##KIND,       \
            .loglevel = LOG_LEVEL_SILENT,        \
            .fd = STDERR_FILENO,                 \
        };                                       \
        tplog_write(LPROBE(), &lm, __VA_ARGS__); \
    }

/* Undefine liblog public macros from liblog to be re-defined */
#undef LOGV
#undef LOGD
#undef LOGI
#undef LOGW
#undef LOGE
#undef LOGC

#define LOGV(...) TP_LOG(VERBOSE, __VA_ARGS__)
#define LOGD(...) TP_LOG(DEBUG, __VA_ARGS__)
#define LOGI(...) TP_LOG(INFO, __VA_ARGS__)
#define LOGW(...) TP_LOG(WARNING, __VA_ARGS__)
#define LOGE(...) TP_LOG(ERROR, __VA_ARGS__)
#define LOGC(...) TP_LOG(CRITICAL, __VA_ARGS__)

/******************************************************************************
* Copyright (C) 2024 michael@ambrus.se Helsinova.se, Helsingborg Sweden
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
******************************************************************************/

#endif // tplog_h
