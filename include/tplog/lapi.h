/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/

#ifndef lua_api_tplog_h
#define lua_api_tplog_h

#include <xb-lua/lua.h>
#include <xb-lua/lauxlib.h>

void bind_tplog_library(lua_State *L);
void bind_tplog_ctrl_library(lua_State *L);
void bind_liblog_ctrl_library(lua_State *L);

#endif //lua_api_tplog_h
