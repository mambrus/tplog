/***************************************************************************
 *   Copyright (C) 2024 Michael Ambrus                                     *
 *                                                                         *
 *   See Copyright Notice in log.h                                         *
 *                                                                         *
 **************************************************************************/
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tplog/log.h>
#include <utrace/utrace.h>

#include "tplog_config.h"
#include "local.h"

#define __init __attribute__((constructor))
#define __fini __attribute__((destructor))

static int init_cntr = 0;
extern struct probe_meta *__start_uprobes;

int tplog_init_cnt()
{
    return init_cntr;
}

/* Module initializers/de-initializers. When used as library (who don't have
 * a natural entry/exit function) these are used to initialize
 * de-initialize. Use to set predefined/default states and cleanup.
 *
 * This will work with shared tplograries as well as with static as they get
 * invoked by RTL load/unload, with or without C++ code (i.e. functions will
 * play nice with C++ normal ctors/dtors).
 *
 * Keep log in to at least once per new build-/run-environment assert that
 * the mechanism works.
 */

void __init __tplog_init(void)
{
    init_cntr++;
#ifdef TPLOG_PRINTFDEBUG
    tplog_open_printfd(TPLOG_PRINTF_FILE);
#endif

#ifdef ENABLE_INITFINI_SHOWEXEC
    struct probe_meta **uprobe = &__start_uprobes;
    log_info("%s [%s]: initializing for: %s\n", TPL_PROJ_NAME, TPL_VERSION, TPL_PROJ_NAME);
    log_info(TPL_PROJ_NAME ": Probe size in .text section is circa [%d] bytes\n",
             uprobe[0]->prsz);
#endif
#ifdef ENABLE_INITFINI_SHOWEXEC
    log_info(TPL_PROJ_NAME ": Register Lua API modules\n");
#endif
    tplog_register_lbms();
#ifdef ENABLE_INITFINI_SHOWEXEC
    log_info(TPL_PROJ_NAME ": Auto initialization completed successfully\n");
#endif
}

void __fini __tplog_fini(void)
{
    init_cntr--;
#ifdef ENABLE_INITFINI_SHOWEXEC
    log_info("% [%s]: de-initializing for: %s\n", TPL_PROJ_NAME, TPL_VERSION,
             TPL_PROJ_NAME);
    fflush(NULL);
#endif
}
