include(CheckLibraryExists)
include(CheckTypeSize)
include(CheckCSourceCompiles)
include(CheckIncludeFiles)
include(CheckFunctionExists)
include(CheckSymbolExists)
include(GNUInstallDirs)

CHECK_LIBRARY_EXISTS(rt clock_gettime "" HAVE_LIB_RT)
CHECK_LIBRARY_EXISTS(pthread pthread_create "" HAVE_LIB_PTHREAD)

if (HAVE_LIB_RT)
    set(EXTRA_LIBS ${EXTRA_LIBS} rt)
endif ()
if (HAVE_LIB_PTHREAD)
    set(EXTRA_LIBS ${EXTRA_LIBS} pthread)
endif ()

CHECK_LIBRARY_EXISTS(sotcptap init_server "" HAVE_LIB_TCPTAP_SHARED)
if (HAVE_LIB_TCPTAP_SHARED)
    set(LIBS_SO ${LIBS_SO} sotcptap)
else ()
    message (WARNING
        "TCP-TAP (so) not found. Will not build what's dependent of TCP-TAP (so)")
endif ()

FIND_LIBRARY(HAVE_LIB_TCPTAP_STATIC atcptap)
FIND_LIBRARY(HAVE_LIB_LIBLOG_STATIC liblog_static)
if (HAVE_LIB_TCPTAP_STATIC AND HAVE_LIB_LIBLOG_STATIC)
    set(LIBS_A ${LIBS_A} atcptap)
    set(LIBS_A ${LIBS_A} liblog_static)
else ()
    message (WARNING
        "TCP-TAP (a) not found. Will not build what's dependent of TCP-TAP (a)")
endif ()

FIND_LIBRARY(HAVE_LIB_LIBLOG liblog)
if (HAVE_LIB_LIBLOG)
    set(LIBS_SO ${LIBS_SO} liblog)
else ()
    message (WARNING
        "liblog (so) not found. Will not build what's dependent of liblog (so)")
endif ()

FIND_LIBRARY(HAVE_LIB_XB_LUACONSOLE xb_luaconsole)
FIND_LIBRARY(HAVE_LIB_XB_LUAAUX xb_luaaux)
FIND_LIBRARY(HAVE_LIB_XB_LUACORE xb_luacore)
FIND_LIBRARY(HAVE_LIB_XB_LUALIB xb_lualib)

if (HAVE_LIB_XB_LUACONSOLE AND
        HAVE_LIB_XB_LUAAUX AND
        HAVE_LIB_XB_LUACORE AND
        HAVE_LIB_XB_LUALIB)

    set(LIBS_SO ${LIBS_SO} xb_luaconsole)
    set(LIBS_SO ${LIBS_SO} xb_luaaux)
    set(LIBS_SO ${LIBS_SO} xb_luacore)
    set(LIBS_SO ${LIBS_SO} xb_lualib)
else ()
    message (WARNING
        "XB_LUA-CONSOLE (so) not found. Will not build what's dependent of XB_LUA-CONSOLE (so)")
endif ()


FIND_LIBRARY(HAVE_LIB_XB_LUACONSOLE_STATIC xb_luaconsole_static)
FIND_LIBRARY(HAVE_LIB_XB_LUAAUX_STATIC xb_luaaux_static)
FIND_LIBRARY(HAVE_LIB_XB_LUACORE_STATIC xb_luacore_static)
FIND_LIBRARY(HAVE_LIB_XB_LUALIB_STATIC xb_lualib_static)

if (HAVE_LIB_XB_LUACONSOLE_STATIC AND
        HAVE_LIB_XB_LUAAUX_STATIC AND
        HAVE_LIB_XB_LUACORE_STATIC AND
        HAVE_LIB_XB_LUALIB_STATIC)

    set(LIBS_A ${LIBS_A} xb_luaconsole_static)
    set(LIBS_A ${LIBS_A} xb_luaaux_static)
    set(LIBS_A ${LIBS_A} xb_luacore_static)
    set(LIBS_A ${LIBS_A} xb_lualib_static)
else ()
    message (WARNING
        "XB_LUA-CONSOLE (a) not found. Will not build what's dependent of XB_LUA-CONSOLE (a)")
endif ()

FIND_LIBRARY(HAVE_LIB_UTRACE utrace)
if (HAVE_LIB_UTRACE)
    set(LIBS_SO ${LIBS_SO} utrace)
else ()
    message (WARNING
        "UTRACE (so) not found. Will not build what's dependent of UTRACE (so)")
endif ()


FIND_LIBRARY(HAVE_LIB_UTRACE_STATIC utrace_static)
if (HAVE_LIB_UTRACE_STATIC)
    set(LIBS_A ${LIBS_A} utrace_static)
else ()
    message (WARNING
        "UTRACE (a) not found. Will not build what's dependent of UTRACE (a)")
endif ()


