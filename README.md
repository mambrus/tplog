# tplog

[`tplog`](https://gitlab.com/mambrus/tplog) <i>"**T**race**P**robe enhanced Logger"</i>
is a thin layer around [`liblog`](https://gitlab.com/mambrus/liblog) and fully plug-and-place
interchangeable for applications using `liblog`.

For `ldoc` generated documentation, see
[link](http://www.ambrus.se/~michael/ldoc/tplog/)

## What `tplog` provides

* Enabling/disabling log-points can be controlled in runtime
* Provides a Lua 5.3 (`xb-lua 5.3.3`) interface for the control
* LOG-points can double as trace-points even when they are not logging.

## Congestion

I.e. the project is a congestion the following projects which it depends on
and uses.

* [tplog](https://gitlab.com/mambrus/tplog)
* [liblog](https://gitlab.com/mambrus/liblog)
* [utrace](https://gitlab.com/mambrus/utrace)
* [xb-lua](https://gitlab.com/mambrus/xb-lua)
* [tcp-tap](https://gitlab.com/mambrus/tcp_tap) (optional and for systems having
  an IP-stack)

The list above somewhat depicts a typical stack, with the application on top
missing. I.e. all these packages are "middleware" and none is an
application. Except for `tcp_tap` who's application can be used to extend
any existing stdio basted applications into a multi-session capable
TCP-service.

But in essence the stack above is how it's used in any normal application to
full-fill the need of having a high performance controllable logger (with
RT-tracing as an added spin-off feature).

## History and why

`tplog` began as a branch under
[`liblog:b-utrace`](https://gitlab.com/mambrus/liblog/-/tree/utrace?ref_type=heads)
which drew it's inspiration from the Android system logger (hence the concept of
log-level and the compatible macros). It was supposed to be a natural
continuation of `liblog`.

The main feature was the ability to control logs post-build/post-deploy, as
there is a trade-off between the use logs provides and the drawback with too
much logging.

`utrace` soon became a project of it's own as it's extremely light-weight
and like `xb-lua` has a natural aim at tiny uC-systems (bare silicon without
OS). But it turned out that having this main-feature embedded in a project
with vastly different aims was a real challenge. The reason is spelled
<i>"dependencies"</i> - in many forms, some of them listed below. It's very
easy to end up with either of these:

* Circular build dependencies (Who builds who)
* Circular run-time dependencies. lib depends on lib in a circular loop.
* Runtime dependencies controlling the probes. To control a probe, which
  is the back-bone of controlling also a log-point, access and permission to
  it's memory is needed. `ptrace` based solutions were discarded due to that
  `ptrace` puts an implicit requirement on capabilities on systems (all bare
  silicon OS-less are automatically excluded).
* Most SW-tracers are also unbearably
  slow compared to what `utrace` can provide and speed matters.
* Hence xb-lua and it's VM and
  console extension to the original Lua. But for that to work, a VM can't be
  generic, i.e. Lua's require can't dynamically link shared libraries that with
  symbol dependencies going in the wrong direction.
* Name collisions, both for API and for DSO's

Most of the above quirks be handled, but it's just way much work with for
comparably little gain. <i>(At least compared to this project...)</i>

## KISS

The simplest solution permitting all the benefits of each project above is
splitting into one more project, this one and to permit true **composition**.

Besides composing a logger/tracer according to exactly the needs and abilities
of a system, it has yet one more mayor benefit:

`liblog` and `tplog` can coexist on the same system. I.e. applications can
either use one, the other or (actually) both.

That's not too bad added benefit from a project counting it's lines in ~100 LOC.

