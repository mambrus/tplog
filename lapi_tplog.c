/***
Lua API for `tplog` based log-probes

`tplog` stands for _"Trace-point logs"_ and is a way to extend log-entries
to become "probes" that can found and have it's behaviour modified in
runtime.

## See also (project home-sites)

- [liblog](https://gitlab.com/mambrus/liblog)
- [utrace](https://gitlab.com/mambrus/utrace)
- [xb-lua](https://gitlab.com/mambrus/xb-lua)
- [tplog](https://gitlab.com/mambrus/tplog)

@module tplog
@author Michael Ambrus <michael@ambrus.se>
@copyright &copy; Michael Ambrus 2024
@license MIT
*/
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <tplog/log.h>
#include <utrace/utrace.h>
#include <xb-lua/luabind.h>

#include "local.h"

static size_t log_ntot();

/***
version string

@function version
@treturn string version
*/
static int l_version(lua_State *L)
{
    lua_pushstring(L, tplog_version());

    return 1;
}

/***
is module initialized

@function isinit
@treturn integer Balance, 0 if un-initialized.
*/
static int l_isinit(lua_State *L)
{
    lua_pushinteger(L, tplog_init_cnt());

    return 1;
}

/***
print all the LOG unconditionally

@function listall
*/
static int l_listall(lua_State *L)
{
    UNUSED(L);
    size_t tot = probe_ntot();
    struct probe_meta **uprobe = probe_start_address();
    char kind; /* What kind of probe */

    for (size_t i = 0; i < tot; i++) {
        if (uprobe[i]->kind == 'L') {
            struct log_meta *lm = (struct log_meta *)(uprobe[i]->piggy);
            kind = lm ? tplog_letter(lm->ploglevel) : uprobe[i]->kind;
            printf("%p %c %s %s %d\n", uprobe[i], kind, uprobe[i]->file,
                   uprobe[i]->func, uprobe[i]->line);
        }
    }
    return 0;
}

/***
Get all probes into a table

Same as "listall" but instead of dumping to stdout, "all" returns a table and
the number of elements in it. This makes furter processing in Lua possible.

@usage
Each element in uprobe-table:
--[[
1. (intp)   ID          i.e. address of probe
2. (char)   kind        V-E ie. VERBOSE to ERROR, or L=Not yet passed (unknown)
3. (char*)  file-name
4. (char*)  function-name
5. (size_t) line-number
--]]

@usage
-- Example. Print a list similar to probe.listall()

n,up = log.all()
for i=1,n do print(up[i][1], up[i][2], up[i][3], up[i][4], up[i][5]) end

@function all
@treturn integer number of probes
@treturn table uprobe-table
*/
static int l_all(lua_State *L)
{
    int nrow = 0;
    size_t nprobes = probe_ntot();
    size_t nlogs = log_ntot();
    struct probe_meta **uprobe = probe_start_address();
    char *rs;
    char kind[5] = { 0 };

    lua_pushinteger(L, log_ntot());
    if (nlogs == 0)
        return 1;

    lua_newtable(L);
    for (size_t i = 0; i < nprobes; i++) {
        if (uprobe[i]->kind == 'L') {
            lua_newtable(L);

            lua_pushinteger(L, (iptr)uprobe[i]);
            lua_rawseti(L, -2, 1);

            struct log_meta *lm = (struct log_meta *)(uprobe[i]->piggy);

            /* Pick-up log-level ('V'-'E') or 'L' if yet not known (i.e.
             * never passed )*/
            kind[0] = lm ? tplog_letter(lm->ploglevel) : uprobe[i]->kind;

            /* Pick-up log-level when under control ('V'-'E') or 'L' if yet
             * not known (i.e. never passed )*/
            kind[1] = lm ? tplog_letter(lm->loglevel) : uprobe[i]->kind;

            rs = (char *)lua_pushstring(L, kind);
            assert(rs);
            lua_rawseti(L, -2, 2);

            rs = (char *)lua_pushstring(L, uprobe[i]->file);
            assert(rs); /* Confirm that the Lua-VM has accepted the string */
            lua_rawseti(L, -2, 3);

            rs = (char *)lua_pushstring(L, uprobe[i]->func);
            assert(rs); /* Confirm that the Lua-VM has accepted the string */
            lua_rawseti(L, -2, 4);

            lua_pushinteger(L, uprobe[i]->line);
            lua_rawseti(L, -2, 5);

            /* Finalize row in parent table */
            lua_rawseti(L, -2, 1 + nrow);
            nrow++;
        }
    }
    return 2;
}

static size_t log_ntot()
{
    size_t n = 0, tot = probe_ntot();
    struct probe_meta **uprobe = probe_start_address();

    for (size_t i = 0; i < tot; i++) {
        if (uprobe[i]->kind == 'L') {
            n++;
        }
    }
    return n;
}

/***
total number of probes

@function total
@treturn integer number of probes
*/
static int l_total(lua_State *L)
{
    lua_pushinteger(L, log_ntot());

    return 1;
}

/***
enable log

Puts a log-point under our control and forces it to either log, silence or
leave supervision-mode. A probe_id is checked for magic markers before it's
state is made. Return-value reflects success or failure. On failure reason
is output on stderr

@function enable
@tparam integer probe_id
@tparam integer control 0=disable, 1=enable, 2=abandon (back to normal)
@treturn bool success
*/
static int l_enable(lua_State *L)
{
    int op;

    uptr iaddr = luaL_checknumber(L, 1);
    struct probe_meta *id = (struct probe_meta *)iaddr;
    struct probe_data *data = id->data;

    op = luaL_checknumber(L, 2);
    if (op < 0 || op > 2) {
        fprintf(stderr, "Bad OP. Expected 0-2\n");
        lua_pushinteger(L, 0);
        return 1;
    }
    if (id->kind != 'L') {
        fprintf(stderr, "%p is not a log-probe. Refusing to touch it\n", id);
        lua_pushinteger(L, 0);
        return 1;
    }
    switch (op) {
    case 0:
        data->status = (data->status | UTRACE_LOG_CNTRL) & ~UTRACE_DO_LOG;
        break;
    case 1:
        data->status = (data->status | UTRACE_LOG_CNTRL) | UTRACE_DO_LOG;
        break;
    case 2:
        data->status = data->status & ~UTRACE_LOG_CNTRL;
        break;
    }

    lua_pushinteger(L, 1);
    return 1;
}

/* Registration part below */
/* ------------------------------------------------------------------------- */

static const struct luaL_Reg this_lib[] = {
    /* clang-format off */
    { "version", l_version },
    { "isinit", l_isinit },
    { "listall", l_listall },
    { "total", l_total },
    { "enable", l_enable },
    { "all", l_all },
    { NULL, NULL }                   /* sentinel */
    /* clang-format on */
};

/* Lua or LBM uses this to register the library in a VM */
static int do_reg(lua_State *L)
{
    luaL_newlib(L, this_lib);
    return 1;
}

/* LBM uses this to register the library in a VM */
void bind_tplog_library(lua_State *L)
{
    luaL_requiref(L, "log", do_reg, 1);
    lua_pop(L, 1);
}
