/***
Lua API for liblog control

Support library for controlling `liblog` using Lua

## See also (project home-sites)

- [liblog](https://gitlab.com/mambrus/liblog)
- [utrace](https://gitlab.com/mambrus/utrace)
- [xb-lua](https://gitlab.com/mambrus/xb-lua)
- [tplog](https://gitlab.com/mambrus/tplog)

@module liblog-ctrl
@author Michael Ambrus <michael@ambrus.se>
@copyright &copy; Michael Ambrus 2024
@license MIT
*/
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <liblog/log.h>
#include <xb-lua/luabind.h>

#ifndef UNUSED
#    define UNUSED(X) ((void)(X))
#endif

static const char *level2str(log_level ll)
{
    switch (ll) {
    case LOG_LEVEL_VERBOSE:
        return "verbose";
        break;
    case LOG_LEVEL_DEBUG:
        return "debug";
        break;
    case LOG_LEVEL_INFO:
        return "info";
        break;
    case LOG_LEVEL_WARNING:
        return "warning";
        break;
    case LOG_LEVEL_ERROR:
        return "error";
        break;
    case LOG_LEVEL_CRITICAL:
        return "critical";
        break;
    case LOG_LEVEL_SILENT:
        return "silent";
        break;
    default:
        return "UNKNOWN";
        break;
    }
    return NULL;
}

/***
version string

@function version
@treturn string version
*/
static int l_version(lua_State *L)
{
    lua_pushstring(L, liblog_version());

    return 1;
}

/***
Get current log-level

@function level
@treturn integer log-level
@treturn string log-level
*/
static int l_level(lua_State *L)
{
    log_level ll = liblog_get_verbosity();

    lua_pushinteger(L, ll);
    lua_pushstring(L, level2str(ll));

    return 2;
}

/***
Set log-level

@function setlevel
@tparam integer new log-level
@treturn integer previous log-level
@treturn integer current log-level
*/
static int l_setlevel(lua_State *L)
{
    log_level nl = luaL_checkinteger(L, 1);
    if ((nl < LOG_LEVEL_VERBOSE) || (nl > LOG_LEVEL_SILENT)) {
        fprintf(stderr, "Invalid argument: %d\n", nl);
        return 0;
    }
    lua_pushinteger(L, liblog_get_verbosity());
    liblog_set_verbosity(nl);
    lua_pushinteger(L, liblog_get_verbosity());

    return 2;
}

/***
Set log-level string

Set log-level as human readable string

@function setslevel
@tparam integer new log-level
@treturn string previous log-level
@treturn string current log-level
*/
static int l_setslevel(lua_State *L)
{
    int ok;
    const char *sll = luaL_checkstring(L, 1);
    log_level nl = liblog_str2loglevel(sll, &ok);

    if (!ok || (nl < LOG_LEVEL_VERBOSE) || (nl > LOG_LEVEL_SILENT)) {
        fprintf(stderr, "Invalid argument: \"%s\"\n", sll);
        return 0;
    }

    lua_pushstring(L, level2str(liblog_get_verbosity()));
    liblog_set_verbosity(nl);
    lua_pushstring(L, level2str(liblog_get_verbosity()));

    return 2;
}

/***
Get liblog settings into a "json"-like table

Each table index is a tuple:
["name" : value]

@usage
Return table
--[[
1. (char *) fname       Write to file-name
2. (enum)   ts          Time-stamp decoration type
        NO_TIME,            Don't decorate with time stamp
        SHORT_TIME,         Numerical epoc timestamp
        VERBOSE_TIME,       HH:MM:SS.ms
        DATE_TIME           Date format
3. (enum)   pidtid      Decorate with process- thread-id
        NO_PIDTID_DECO,     Don't decorate with pid/tid
        TID_DECO,           Thread only (suitable when syslog as only pid is shown)
        PID_DECO,           PID only (quite useless)
        PIDTID_DECO         Both pid/tid
4. (bool)   tosyslog    Send to syslog without decoration
5. (bool)   sys2err     If syslog, also stderr (syslogs decoration)
6. (char *) proc_name   Name of process
7. (int)    nsync       Flush output after each write in File-IO mode
                           0 :  buffered
                          <0 :  Flush file after each write (warning:
                                decreases performance)
                          >0 :  Flush file after each nsync write
--]]

@usage
-- Example of usage:
    n,s = liblog.settings()
    for i=1,n do print(s[i][1], s[i][2]) end

@function settings
@treturn integer number settings
@treturn table tuple-table with settings
*/
static int l_settings(lua_State *L)
{
    char *rs;
    struct liblog_settings settings = liblog_settings();
    UNUSED(L);
    UNUSED(rs);

    lua_pushinteger(L, 7);

    lua_newtable(L);
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "fname");
        assert(rs);
        lua_rawseti(L, -2, 1);

        rs = (char *)lua_pushstring(L, settings.fname);
        assert(rs);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 1); /* Finalize row */
    }
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "ts");
        assert(rs);
        lua_rawseti(L, -2, 1);

        lua_pushinteger(L, settings.ts);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 2); /* Finalize row */
    }
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "pidtid");
        assert(rs);
        lua_rawseti(L, -2, 1);

        lua_pushinteger(L, settings.ptid);
        assert(rs);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 3); /* Finalize row */
    }
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "tosyslog");
        assert(rs);
        lua_rawseti(L, -2, 1);

        lua_pushinteger(L, settings.tosyslog);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 4); /* Finalize row */
    }
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "sys2err");
        assert(rs);
        lua_rawseti(L, -2, 1);

        lua_pushinteger(L, settings.sys2err);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 5); /* Finalize row */
    }
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "proc_name");
        assert(rs);
        lua_rawseti(L, -2, 1);

        rs = (char *)lua_pushstring(L, settings.proc_name);
        assert(rs);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 6); /* Finalize row */
    }
    {
        lua_newtable(L);
        rs = (char *)lua_pushstring(L, "sync");
        assert(rs);
        lua_rawseti(L, -2, 1);

        lua_pushinteger(L, settings.sys2err);
        lua_rawseti(L, -2, 2);

        lua_rawseti(L, -2, 7); /* Finalize row */
    }

    return 2;
}

/***
Set usage of syslog (mode)

Enable syslog or close it (if opened) and go back to file-operation mode.

Which is determined my the first argument. Second argument enables copy
syslog to stderr. It's ignored if not enabling syslog operation.

@function setsyslog
@tparam boolean Enable 1=Enable 0=Disable
@tparam boolean stderrdup 1=Enable 0=Disable
@treturn boolean previous mode-state
@treturn boolean previous sys2err
@treturn boolean current state-state
@treturn boolean previous sys2err
*/
static int l_setsyslog(lua_State *L)
{
    bool is_good = true;
    bool syslog;
    bool sys2err;

    syslog = luaL_checkinteger(L, 1);
    if ((syslog < 0) || (syslog > 1)) {
        fprintf(stderr, "Arg1 invalid: %d\n", syslog);
        is_good = false;
    }
    sys2err = luaL_checkinteger(L, 2);
    if ((sys2err < 0) || (sys2err > 1)) {
        fprintf(stderr, "Arg1 invalid: %d\n", sys2err);
        is_good = false;
    }
    if (!is_good)
        return 0;

    struct liblog_settings settings;

    /* Read and push */
    settings = liblog_settings();
    lua_pushinteger(L, settings.tosyslog);
    lua_pushinteger(L, settings.sys2err);

    if (syslog)
        liblog_syslog_config(sys2err);
    else
        liblog_syslog_close();

    /* Read-back and push */
    settings = liblog_settings();
    lua_pushinteger(L, settings.tosyslog);
    lua_pushinteger(L, settings.sys2err);

    return 4;
}

/***
Set File-IO mode filename

Enable File-IO mode with file-name
Disable syslog or close it (if opened)

Which is determined by the first argument. Second argument enables copy
syslog to stderr. It's ignored if not enabling syslog operation.

@function setlogfile
@tparam string filename
@tparam nsync  Sync counter. 0=Buffered, -1=Fully sync, >0 sync each n'th

@treturn string previous filename
@treturn integer previous sync-mode/-cntr
@treturn string current filename
@treturn integer current sync-mode/-cntr
*/
static int l_setlogfile(lua_State *L)
{
    const char *fname = luaL_checkstring(L, 1);
    struct liblog_settings settings;
    const char *rc;
    UNUSED(rc);

    /* Read and push current settings */
    settings = liblog_settings();
    rc = lua_pushstring(L, settings.fname);
    assert(rc);
    lua_pushinteger(L, settings.nsync);

    int nsync = luaL_checkinteger(L, 2);
    liblog_logfile(fname, nsync);

    /* Read and push new settings */
    settings = liblog_settings();
    rc = lua_pushstring(L, settings.fname);
    assert(rc);
    lua_pushinteger(L, settings.nsync);

    return 4;
}

/***
Set decorators

For values:

@see settings

@function decorate
@tparam integer  Decorate time-stamp mode. 0=none, 1=Numerical...
@tparam integer  Decorate pid/tid mode. 0=off, 1=tid...

@treturn integer previous ts
@treturn integer previous pidtid
@treturn integer current ts
@treturn integer current pidtid
*/
static int l_setdeco(lua_State *L)
{
    bool is_good = true;
    int ts_val;
    int ptid_val;

    ts_val = luaL_checkinteger(L, 1);
    if ((ts_val < 0) || (ts_val > 4)) {
        fprintf(stderr, "Arg1 invalid: %d\n", ts_val);
        is_good = false;
    }
    ptid_val = luaL_checkinteger(L, 2);
    if ((ptid_val < 0) || (ptid_val > 4)) {
        fprintf(stderr, "Arg1 invalid: %d\n", ptid_val);
        is_good = false;
    }
    if (!is_good)
        return 0;

    struct liblog_settings settings;

    /* Read and push current */
    settings = liblog_settings();
    lua_pushinteger(L, settings.ts);
    lua_pushinteger(L, settings.ptid);

    liblog_set_decorators(ts_val, ptid_val);

    /* Read-back and push */
    settings = liblog_settings();
    lua_pushinteger(L, settings.ts);
    lua_pushinteger(L, settings.ptid);

    return 4;
}

/***
Set/unset process/marker decorator

This is a special decorator as it can be any string. Usually it contains
application or process name but user can but anything here.

@function setname
@tparam string  Any string (marker)

@treturn string previous marker
@treturn string current marker
*/
static int l_setname(lua_State *L)
{
    const char *name = luaL_checkstring(L, 1);
    struct liblog_settings settings;

    settings = liblog_settings();
    lua_pushstring(L, settings.proc_name);

    if ((name == NULL) || (strlen(name) == 0))
        liblog_set_process_name(NULL);
    else
        liblog_set_process_name(name);

    settings = liblog_settings();
    lua_pushstring(L, settings.proc_name);

    return 2;
}

static bool send_log(log_level ll, const char *msg)
{
    bool is_good = true;

    switch (ll) {
    case LOG_LEVEL_VERBOSE:
        LOGV("%s\n", msg);
        break;
    case LOG_LEVEL_DEBUG:
        LOGD("%s\n", msg);
        break;
    case LOG_LEVEL_INFO:
        LOGI("%s\n", msg);
        break;
    case LOG_LEVEL_WARNING:
        LOGW("%s\n", msg);
        break;
    case LOG_LEVEL_ERROR:
        LOGE("%s\n", msg);
        break;
    case LOG_LEVEL_CRITICAL:
        log_critical("%s\n", msg);
        break;
    case LOG_LEVEL_SILENT:
    default:
        is_good = false;
        break;
    }
    return is_good;
}

/***
Send log

Test-API to send a liblog log-message of certain type
Disable syslog or close it (if opened)

Which type determined by the first argument. Second argument enables copy
syslog to stderr. It's ignored if not enabling syslog operation.

@function send
@tparam integer  V=0, D=1, I=2, W=3, E=4, C=5
@tparam string Message

@treturn boolean OK
*/
static int l_send(lua_State *L)
{
    bool is_good = true;
    int ll;

    ll = luaL_checkinteger(L, 1);
    if ((ll < 0) || (ll > 5)) {
        fprintf(stderr, "Arg1 invalid: %d\n", ll);
        is_good = false;
    }

    if (!is_good) {
        lua_pushinteger(L, 0);
        return 1;
    }

    const char *msg = luaL_checkstring(L, 2);
    is_good = send_log(ll, msg);

    if (is_good)
        lua_pushinteger(L, 1);
    else
        lua_pushinteger(L, 0);

    return 1;
}

/***
Send log message

Test-API to send a liblog log-message of certain type

Which type to send is determined by the first argument.

@function log
@tparam string Type "VERBOSE", "DEBUG", "INFO", "ERROR", "CRITICAL"
@tparam string Message

@treturn boolean OK
*/
static int l_log(lua_State *L)
{
    int is_good = true;
    const char *level = luaL_checkstring(L, 1);
    log_level ll = liblog_str2loglevel(level, &is_good);

    if (!is_good) {
        fprintf(stderr, "Arg1 invalid: %s\n", level);
        lua_pushinteger(L, 0);
        return 1;
    }

    const char *msg = luaL_checkstring(L, 2);
    is_good = send_log(ll, msg);

    if (is_good)
        lua_pushinteger(L, 1);
    else
        lua_pushinteger(L, 0);

    return 1;
}

/* Registration part below */
/* ------------------------------------------------------------------------- */

static const struct luaL_Reg this_lib[] = {
    /* clang-format off */
    {"version", l_version},
    {"level", l_level},
    {"setlevel", l_setlevel},
    {"setslevel", l_setslevel}, /* Human version of setlevel() */
    {"settings", l_settings},
    {"setsyslog", l_setsyslog},
    {"setlogfile", l_setlogfile},
    {"setdeco", l_setdeco},
    {"setname", l_setname},
    {"send", l_send},
    {"log", l_log},
    {NULL, NULL}                /* sentinel */
    /* clang-format on */
};

/* Lua or LBM uses this to register the library in a VM */
static int do_reg(lua_State *L)
{
    luaL_newlib(L, this_lib);
    return 1;
}

/* LBM uses this to register the library in a VM */
void bind_liblog_ctrl_library(lua_State *L)
{
    luaL_requiref(L, "liblog", do_reg, 1);
    lua_pop(L, 1);
}
